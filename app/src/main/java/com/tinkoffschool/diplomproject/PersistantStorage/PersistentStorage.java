package com.tinkoffschool.diplomproject.PersistantStorage;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import com.tinkoffschool.diplomproject.MyApp.MyApp;

public class PersistentStorage {
    private static PersistentStorage persistentStorage;



    // имя настройки
    private final String STORAGE_NAME = "mysettings";
    public final String APP_PREFERENCES_A_TOKEN = "TokenA";
    public final String APP_PREFERENCES_R_TOKEN = "TokenR";

    private SharedPreferences settings = null;
    private SharedPreferences.Editor editor = null;
    private static Context context;

    private PersistentStorage(){
  }
    public static PersistentStorage getInstance(Context contextApp){ // #3
        if(persistentStorage == null){		//если объект еще не создан
           context=contextApp;
            persistentStorage = new PersistentStorage();	//создать новый объект
        }
        return persistentStorage;		// вернуть ранее созданный объект
    }

    private void init() {
        settings = context.getSharedPreferences(STORAGE_NAME, Context.MODE_PRIVATE);
        editor = settings.edit();
    }

    public void addProperty(String name, String value) {
        if (settings == null) {
            init();
        }
        editor.putString(name, value);
        editor.apply();
    }

    public String getProperty(String name) {
        if (settings == null) {
            init();
        }
        return settings.getString(name, null);
    }

    public void removeProperty() {
        editor.remove(APP_PREFERENCES_A_TOKEN);
        editor.remove(APP_PREFERENCES_R_TOKEN);
        editor.clear();
        editor.apply();
    }
}


