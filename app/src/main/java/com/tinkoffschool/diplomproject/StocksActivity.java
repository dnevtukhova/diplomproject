package com.tinkoffschool.diplomproject;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.arch.paging.PagedList;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;

import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.tinkoffschool.diplomproject.Adapter.RVAdapter;
import com.tinkoffschool.diplomproject.PagingLibrary.ContextManager;
import com.tinkoffschool.diplomproject.PagingLibrary.ForStocks.PagedAdapterStocks;
import com.tinkoffschool.diplomproject.PagingLibrary.ForStocks.StocksViewModel;
import com.tinkoffschool.diplomproject.PersistantStorage.PersistentStorage;
import com.tinkoffschool.diplomproject.Retrofit.ServerApi;
import com.tinkoffschool.diplomproject.Retrofit.Stocks;
import com.tinkoffschool.diplomproject.Retrofit.StocksBag;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.tinkoffschool.diplomproject.LoginActivity.BASE_URL;

public class StocksActivity extends AppCompatActivity {
    TextView textViewNamePage;
   // private MenuItem searchMenuItem;
   // private SearchView mSearchView;
   // private ImageButton buttonBackStocks;
    private ProgressBar progressBarStocks;
    PersistentStorage persistentStorage;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.stocks_menu, menu);
        MenuItem searchMenuItem = menu.findItem(R.menu.stocks_menu);
        /*searchMenuItem.getActionView();*/
        SearchView mSearchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.action_search));

        mSearchView.setOnQueryTextFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    final RecyclerView rv = findViewById(R.id.rvStocks);
                    callApiStocks(rv);

                }
            }
        });
        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                stocksSearchApi(s);
                return false;
            }
        });
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stocks);
        ContextManager.setContext(this);

        //toolbar
        Toolbar mActionBarToolbar = findViewById(R.id.toolbar_stocks);
        setSupportActionBar(mActionBarToolbar);
        textViewNamePage = findViewById(R.id.name_page);
        textViewNamePage.setText(R.string.name_stocks);

        final RecyclerView rv = findViewById(R.id.rvStocks);

        //прогрессБар
        progressBarStocks = findViewById(R.id.stocks_progress);

        //кнопка Назад
        ImageButton buttonBackStocks = findViewById(R.id.back_button);
        buttonBackStocks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), AccountInfoActivity.class);
                startActivity(intent);
                showProgress(false);

            }
        });

        SwipeRefreshLayout mSwipeRefreshLayoutStock = findViewById(R.id.swipeRefreshLayoutStocks);

        callApiStocks(rv);
        onRefreshLayoutStocks(mSwipeRefreshLayoutStock, rv);
    }

    private void stocksSearchApi(String s) //вставить спиннер на форму!!!
    {

        if (s.length() > 2) {
            showProgress(true);
            final RecyclerView rv = findViewById(R.id.rvStocks);
            LinearLayoutManager llm = new LinearLayoutManager(getApplicationContext());
            rv.setLayoutManager(llm);
            //PersistentStorage persistentStorage = new PersistentStorage();
            //получаем токен из персистентного хранилища
           // PersistentStorage.init(getApplicationContext());
            persistentStorage = PersistentStorage.getInstance(getApplicationContext());
            String token = persistentStorage.getProperty(getApplicationContext().getString(R.string.tokenA));

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            final ServerApi serverApi = retrofit.create(ServerApi.class);
            Call<StocksBag> call = serverApi.getStocksSearch(token, 10, s);
            System.out.println(token);
            System.out.println(call);
            call.enqueue(new Callback<StocksBag>() {
                @Override
                public void onResponse(@NonNull Call<StocksBag> call, @NonNull Response<StocksBag> response) {
                    showProgress(true);
                    if (response.isSuccessful()) {
                        System.out.println("response!! " + response.toString());
                        StocksBag stocksBag = response.body();
                        Stocks[] stocks = stocksBag.items;

                        for (int i = 0; i < stocks.length; i++) {
                            System.out.println(stocks[i].code + " " + stocks[i].count + " " + stocks[i].id + " " + stocks[i].price + " " + stocks[i].priceDelta + " " + stocks[i].name + " ");
                        }

                        RVAdapter rvAdapter = new RVAdapter(stocks, getSupportFragmentManager(), rv, getApplicationContext());
                        rv.setAdapter(rvAdapter);
                        rvAdapter.notifyDataSetChanged();

                    } else {
                        System.out.println("responcecode!!!!" + response.code() + response.body()
                        );
                    }
                    showProgress(false);

                }

                @Override
                public void onFailure(@NonNull Call<StocksBag> call,@NonNull Throwable t) {
                    System.out.println("failure!!!! " + /*exception.code()*/t);
                    showProgress(false);
                }
            });


        }
    }

    private void callApiStocks(final RecyclerView rv) {
        showProgress(true);
        LinearLayoutManager llm = new LinearLayoutManager(getApplicationContext());
        rv.setLayoutManager(llm);
       // PersistentStorage persistentStorage = new PersistentStorage();
        //получаем токен из персистентного хранилища
      //PersistentStorage.init(this);
        persistentStorage = PersistentStorage.getInstance(getApplicationContext());
      String token = persistentStorage.getProperty(getApplicationContext().getString(R.string.tokenA));

        StocksViewModel stocksViewModel = ViewModelProviders.of(this).get(StocksViewModel.class);

        //creating the Adapter
        final PagedAdapterStocks adapter = new PagedAdapterStocks(getSupportFragmentManager(), rv, this);


        //observing the itemPagedList from view model
        stocksViewModel.itemPagedList.observe(this, new Observer<PagedList<Stocks>>() {
            @Override
            public void onChanged(@Nullable PagedList<Stocks> items) {

                //in case of any changes
                //submitting the items to adapter
                adapter.submitList(items);
            }
        });

        //setting the adapter
        rv.setAdapter(adapter);
        showProgress(false);
    }

    private void onRefreshLayoutStocks(final SwipeRefreshLayout mSwipeRefreshLayout, final RecyclerView rv) {
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                callApiStocks(rv);
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    private void showProgress(boolean visible) {
        progressBarStocks.setVisibility(visible ? View.VISIBLE : View.INVISIBLE);
    }
}
