package com.tinkoffschool.diplomproject;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.tinkoffschool.diplomproject.PersistantStorage.PersistentStorage;
import com.tinkoffschool.diplomproject.Retrofit.ServerApi;
import com.tinkoffschool.diplomproject.Retrofit.StockIdAmount;
import com.tinkoffschool.diplomproject.Retrofit.TransactionsSellBuy;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.HttpException;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.tinkoffschool.diplomproject.LoginActivity.BASE_URL;

public class FragmentBottomSheetSel extends BottomSheetDialogFragment implements View.OnClickListener {
    Button stocksSell;
    EditText editTextStocksSell;
    TextView stockName;
   // String idStock;
    String count;
    String price;
    String idFr;
    View rootView;

    PersistentStorage persistentStorage;


    public static FragmentBottomSheetSel newInstance() {
        return new FragmentBottomSheetSel();
    }

    public FragmentBottomSheetSel() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.bottom_sheet_buy_fragment, container, false);
        //получам данные из адаптера (название акции)
        Bundle bundle = getArguments();
        String valueFr = bundle.getString("name");

        //получаем данные из адаптера
        idFr = bundle.getString("id");
        price = bundle.getString("price");

        //в общем выводим в текствью название акции
        stockName = rootView.findViewById(R.id.stocks_name_sell_buy);
        stockName.setText(valueFr);

        stocksSell = rootView.findViewById(R.id.button_buy);
        editTextStocksSell = rootView.findViewById(R.id.edit_text_enter_count);
        stocksSell.setText("Продать");
        stocksSell.setOnClickListener(this);

        return rootView;

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onClick(View v) {
        if (editTextStocksSell.getText().length() == 0) {
            Toast.makeText(getActivity(), +R.string.fill_count_stock, Toast.LENGTH_SHORT).show();
        } else {
            count = editTextStocksSell.getText().toString();
            sellStocks(idFr, count);
            Intent intent = new Intent(getActivity(), AccountInfoActivity.class);
            startActivity(intent);

        }

    }

    public void sellStocks(String idStock, String count) {
        StockIdAmount stockIdAmount = new StockIdAmount(Integer.parseInt(idStock), Integer.parseInt(count));
        //получаем токен из персистентного хранилища
        //PersistentStorage.init(getContext());
        persistentStorage = PersistentStorage.getInstance(getActivity().getApplicationContext());
        String token = persistentStorage.getProperty(getActivity().getApplicationContext().getString(R.string.tokenA));
        System.out.println(token);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        final ServerApi serverApi = retrofit.create(ServerApi.class);
        Call<TransactionsSellBuy> call = serverApi.transactionSell(token, stockIdAmount);
        call.enqueue(new Callback<TransactionsSellBuy>() {

            @Override
            public void onResponse(@NonNull Call<TransactionsSellBuy> call, @NonNull Response<TransactionsSellBuy> response) {
                if (response.isSuccessful()) {

                    TransactionsSellBuy transactionsSell = response.body();
                    Toast.makeText(getActivity(), transactionsSell.status, Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getActivity(), R.string.error, Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(@NonNull Call<TransactionsSellBuy> call, @NonNull Throwable t) {
                HttpException exception = (HttpException) t;
                Toast.makeText(getActivity(), "" + t + exception.code(), Toast.LENGTH_SHORT).show();
            }

        });
    }
}


