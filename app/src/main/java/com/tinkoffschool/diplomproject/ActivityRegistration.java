package com.tinkoffschool.diplomproject;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;

import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.tinkoffschool.diplomproject.PersistantStorage.PersistentStorage;
import com.tinkoffschool.diplomproject.Retrofit.ServerApi;
import com.tinkoffschool.diplomproject.Retrofit.Token;
import com.tinkoffschool.diplomproject.Retrofit.User;
import com.tinkoffschool.diplomproject.TextValidator.TextValidator;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.tinkoffschool.diplomproject.LoginActivity.BASE_URL;

import static com.tinkoffschool.diplomproject.NetWorkUtils.NetWorkUtils.hasConnection;


public class ActivityRegistration extends AppCompatActivity /*implements LoaderCallbacks<Cursor> */ {

    private AutoCompleteTextView mLoginView;
    private EditText mPasswordView;
    private View mProgressView;
    private View mLoginFormView;
   PersistentStorage persistentStorage;

    //константы
    private String SUCCESS_PLEASE_AUTHORIZE = "Успешно. Просьба авторизоваться!";
    private String INCORRECT_VALUE_IN_FIELD = "Одно или несколько полей содержат недопустимые значения";
    private String SERVER_ERROR = "Ошибка Сервера";
    private String OTHER_MISTAKE = "Ошибка! Повторите вход!";
    private String REGISTRATION_ERROR = "Возникла ошибка регистрации";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        //кнопка по умолчанию неактивна
        final Button loginSignInButton = findViewById(R.id.email_sign_in_button1);
        loginSignInButton.setEnabled(false);

        mLoginFormView = findViewById(R.id.login_form1);
        mProgressView = findViewById(R.id.login_progress1);

        //настраиваем валидацию полей
        mLoginView = findViewById(R.id.email1);
        mLoginView.addTextChangedListener(new TextValidator(mLoginView) {
            @Override
            public void validate(TextView textView, String text) {

                if (!isEmailValid(textView.getText().toString())) {
                    loginSignInButton.setEnabled(false);
                }
                if (isPasswordValid(mPasswordView.getText().toString())) {
                    loginSignInButton.setEnabled(true);
                }
            }
        });

        mPasswordView = findViewById(R.id.password1);
        mPasswordView.addTextChangedListener(new TextValidator(mPasswordView) {
            @Override
            public void validate(TextView textView, String text) {
                if (!isPasswordValid(textView.getText().toString())) {
                    loginSignInButton.setEnabled(false);
                }
                if (isEmailValid(mPasswordView.getText().toString())) {
                    loginSignInButton.setEnabled(true);
                }

            }
        });

        loginSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                //через retrofit
                showProgress(true);
                // проверяем соединение
                if (!hasConnection(getApplicationContext())) {
                    Toast.makeText(getApplicationContext(),
                            R.string.no_internet, Toast.LENGTH_SHORT).show();
                    showProgress(false);
                } else if (!isPasswordValid(mPasswordView.getText().toString())) {
                    Toast.makeText(getApplicationContext(),
                            R.string.uncorrect_password, Toast.LENGTH_SHORT).show();
                    showProgress(false);
                } else {
                    Retrofit retrofit = new Retrofit.Builder()
                            .baseUrl(BASE_URL)
                            .addConverterFactory(GsonConverterFactory.create())
                            .build();
                    ServerApi serverApi = retrofit.create(ServerApi.class);
                    String entryLogin = mLoginView.getText().toString();
                    String entryPassword = mPasswordView.getText().toString();

                    Call<Token> call = serverApi.signUp(new User(entryLogin, entryPassword));
                    call.enqueue(new Callback<Token>() {
                        @Override
                        public void onResponse(@NonNull Call<Token> call, @NonNull Response<Token> response) {
                            if (response.isSuccessful()) {

                                Token token = response.body();

                               // PersistentStorage.init(getApplicationContext());
                                persistentStorage = PersistentStorage.getInstance(getApplicationContext());
                                persistentStorage.addProperty(getApplicationContext().getString(R.string.tokenA), token.accessToken);
                                persistentStorage.addProperty(getApplicationContext().getString(R.string.tokenR), token.refreshToken);

                                Toast.makeText(getApplicationContext(),
                                        getApplicationContext().getString(R.string.authorization_success) + " " + response.code(), Toast.LENGTH_SHORT).show();

                                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                                intent.putExtra("token", token.accessToken);
                                startActivity(intent);

                            } else if (response.code() == 400) {
                                showProgress(false);
                                Toast.makeText(getApplicationContext(),
                                        getApplicationContext().getString(R.string.incorrect_value_in_field) + " " + response.code(), Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(getApplicationContext(), ActivityRegistration.class);
                                startActivity(intent);
                            } else if (response.code() == 500) {
                                showProgress(false);
                                Toast.makeText(getApplicationContext(),
                                        getApplicationContext().getString(R.string.server_error) + " " + response.code(), Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(getApplicationContext(), ActivityRegistration.class);
                                startActivity(intent);
                            } else {
                                showProgress(false);
                                Toast.makeText(getApplicationContext(),
                                        getApplicationContext().getString(R.string.other_mistake) + " " + response.code(), Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(getApplicationContext(), ActivityRegistration.class);
                                startActivity(intent);
                            }
                        }

                        @Override
                        public void onFailure(@NonNull Call<Token> call, @NonNull Throwable t) {
                            showProgress(false);
                            System.out.println("failure!!!! " + t);
                            Toast.makeText(getApplicationContext(),
                                    getApplicationContext().getString(R.string.registration_error) + " " + t, Toast.LENGTH_SHORT).show();

                        }
                    });

                }
            }
        });

    }

    private boolean isEmailValid(String email) {
        return email.length() > 2;
    }

    private boolean isPasswordValid(String password) {
        return password.length() > 4;
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }
}

