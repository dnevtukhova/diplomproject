package com.tinkoffschool.diplomproject;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.Toast;

import com.tinkoffschool.diplomproject.Caching.Caching;
import com.tinkoffschool.diplomproject.PersistantStorage.PersistentStorage;
import com.tinkoffschool.diplomproject.Retrofit.AccountInfo;
import com.tinkoffschool.diplomproject.Retrofit.RefreshToken;
import com.tinkoffschool.diplomproject.Retrofit.ServerApi;
import com.tinkoffschool.diplomproject.Retrofit.Token;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.tinkoffschool.diplomproject.LoginActivity.BASE_URL;

public class PreloadActivity extends AppCompatActivity {
    private ImageView myImageView;
    PersistentStorage persistentStorage;
    //констаты
//    private String INCORRECT_LOGIN_PASSWORD = "Неверный логин/пароль";
  //  private String SERVER_ERROR = "Ошибка Сервера";
///    private String OTHER_MISTAKE = "Ошибка! Повторите вход!";
 //   private String OOPS_ERROR = "Что-то пошло не так...";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.preloader);
        final Animation animationRotateCenter = AnimationUtils.loadAnimation(
                this, R.anim.rotate_center);
        myImageView = findViewById(R.id.preload_photo);
        // myImageView.setAnimation(animationRotateCenter);
        showAnim(animationRotateCenter);

        final SharedPreferences sp = PreferenceManager
                .getDefaultSharedPreferences(this); //this.getPreferences(Context.MODE_PRIVATE);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                //если отсутствует файл shared preferences
                showAnim(animationRotateCenter);
                if (sp == null) {
                    //   myImageView.setAnimation(animationRotateCenter);
                    Intent i = new Intent(PreloadActivity.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                }
                //если файл существует берем оттуда токены и проверяем
                else {
                    refresh(animationRotateCenter);
                }
            }
        }, 2000);
    }

    private void refresh(final Animation animation) {
        showAnim(animation);
        System.out.println("ТОКЕН"+getApplicationContext().getString(R.string.tokenA));
       persistentStorage = PersistentStorage.getInstance(getApplicationContext());
   //    PersistentStorage.init(getApplicationContext());
        String token = persistentStorage.getProperty(getApplicationContext().getString(R.string.tokenA));
        System.out.println("ТОКЕН"+getApplicationContext().getString(R.string.tokenA));

        //кеш
        OkHttpClient client = new Caching().CachingFile(getApplicationContext());

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        final ServerApi serverApi = retrofit.create(ServerApi.class);
        Call<AccountInfo> call = serverApi.getAccountInfo(token);
        call.enqueue(new Callback<AccountInfo>() {
            @Override
            public void onResponse(@NonNull Call<AccountInfo> call, @NonNull Response<AccountInfo> response) {
                showAnim(animation);
                if (response.isSuccessful()) {

                    Toast.makeText(getApplicationContext(),
                            R.string.authorization_success, Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(getApplicationContext(), AccountInfoActivity.class);
                    startActivity(intent);
                } else if (response.code() == 500) {
                    Toast.makeText(getApplicationContext(),
                            getApplicationContext().getString(R.string.server_error) + " " + response.code() + response.body(), Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                    startActivity(intent);
                } else if (response.code() == 401) {
                    System.out.println("response code!!! " + response.code());
                    //обновляем токен
                    refreshToken();
                } else {
                    Toast.makeText(getApplicationContext(),
                            getApplicationContext().getString(R.string.other_mistake) + "Ошибка, повторите вход " + response.code() + response.body(), Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                    startActivity(intent);
                }
            }

            @Override
            public void onFailure(@NonNull Call<AccountInfo> call, @NonNull Throwable t) {
                Toast.makeText(getApplicationContext(),
                        getApplicationContext().getString(R.string.oops_error) + " " + t, Toast.LENGTH_SHORT).show();

                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(intent);
            }
        });

    }

    private void refreshToken() {
        //кеш
        OkHttpClient client = new Caching().CachingFile(getApplicationContext());

        //PersistentStorage persistentStorage = new PersistentStorage();
        //PersistentStorage.init(getApplicationContext());
        String token = persistentStorage.getProperty(getApplicationContext().getString(R.string.tokenR));
        RefreshToken rToken = new RefreshToken(token);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        final ServerApi serverApi = retrofit.create(ServerApi.class);
        Call<Token> callRefresh = serverApi.refresh(rToken);
        callRefresh.enqueue(new Callback<Token>() {
            @Override
            public void onResponse(@NonNull Call<Token> call, @NonNull Response<Token> response) {
                System.out.println("RESPONSE CODE" + response.code());

                if (response.isSuccessful()) {
                    Token token = response.body();

                    //заносим данные токенов в Shared Preferences
                   // PersistentStorage persistentStorage = new PersistentStorage();
                  //  PersistentStorage.init(getApplicationContext());
                    persistentStorage.addProperty(getApplicationContext().getString(R.string.tokenA), token.accessToken);
                    persistentStorage.addProperty(getApplicationContext().getString(R.string.tokenR), token.refreshToken);

                    Toast.makeText(getApplicationContext(),
                            R.string.authorization_success, Toast.LENGTH_SHORT).show();

                    Intent intent = new Intent(getApplicationContext(), AccountInfoActivity.class);
                    intent.putExtra("token", token.accessToken);
                    startActivity(intent);

                } else if (response.code() == 500) {
                    Toast.makeText(getApplicationContext(),
                            getApplicationContext().getString(R.string.server_error) + " " + response.code() + response.body(), Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                    startActivity(intent);
                } else if (response.code() == 401) {
                    Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                    startActivity(intent);
                } else if (response.code() == 403) {
                    Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                    startActivity(intent);
                } else {
                    Toast.makeText(getApplicationContext(),
                            getApplicationContext().getString(R.string.other_mistake) + " " + response.code() + " " + response.body(), Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                    startActivity(intent);
                }
            }

            @Override
            public void onFailure(@NonNull Call<Token> call, @NonNull Throwable t) {
                Toast.makeText(getApplicationContext(),
                        getApplicationContext().getString(R.string.oops_error) + " " + t, Toast.LENGTH_SHORT).show();

                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(intent);
            }

        });
    }

    public void showAnim(Animation animation) {
        myImageView.setAnimation(animation);
    }

}
