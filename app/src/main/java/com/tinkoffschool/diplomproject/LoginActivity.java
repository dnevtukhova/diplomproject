package com.tinkoffschool.diplomproject;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;

import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.tinkoffschool.diplomproject.PersistantStorage.PersistentStorage;
import com.tinkoffschool.diplomproject.Retrofit.ServerApi;
import com.tinkoffschool.diplomproject.Retrofit.Token;
import com.tinkoffschool.diplomproject.Retrofit.User;
import com.tinkoffschool.diplomproject.TextValidator.TextValidator;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.HttpException;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.tinkoffschool.diplomproject.NetWorkUtils.NetWorkUtils.hasConnection;


public class LoginActivity extends AppCompatActivity {

    // UI references.
    private AutoCompleteTextView mLoginView;
    private EditText mPasswordView;
    private View mProgressView;
    private View mLoginFormView;
   // private TextView createAccount;

    public static String BASE_URL = "https://warm-citadel-97897.herokuapp.com";

    //констаты
    private String INCORRECT_LOGIN_PASSWORD = "Неверный логин/пароль";
    private String SERVER_ERROR = "Ошибка Сервера";
    private String OTHER_MISTAKE = "Ошибка! Повторите вход!";
    private String OOPS_ERROR = "Что-то пошло не так...";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        //кнопка по умолчанию неактивна
        final Button LoginSignInButton = findViewById(R.id.email_sign_in_button);
        LoginSignInButton.setEnabled(false);
        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);
        TextView createAccount = findViewById(R.id.textCreateAccount);

        //настраиваем валидацию кнопка неактивна до момента пока пользователь не введет 4 символа
        mLoginView = findViewById(R.id.email);
        mLoginView.addTextChangedListener(new TextValidator(mLoginView) {
            @Override
            public void validate(TextView textView, String text) {
                if (!isEmailValid(textView.getText().toString())) {
                    LoginSignInButton.setEnabled(false);
                }
                if (isPasswordValid(mPasswordView.getText().toString())) {
                    LoginSignInButton.setEnabled(true);
                }
            }
        });

        mPasswordView = findViewById(R.id.password);
        mPasswordView.addTextChangedListener(new TextValidator(mPasswordView) {
            @Override
            public void validate(TextView textView, String text) {
                if (!isPasswordValid(textView.getText().toString())) {
                    LoginSignInButton.setEnabled(false);
                }
                if (isEmailValid(mPasswordView.getText().toString())) {
                    LoginSignInButton.setEnabled(true);
                }

            }
        });

        //через retrofit
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        final ServerApi serverApi = retrofit.create(ServerApi.class);

        LoginSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(final View view) {

                //через retrofit
                showProgress(true);
                // проверяем соединение
                if (!hasConnection(getApplicationContext())) {
                    Toast.makeText(getApplicationContext(),
                            R.string.no_internet, Toast.LENGTH_SHORT).show();
                    showProgress(false);
                } else if (!isPasswordValid(mPasswordView.getText().toString())) {
                    Toast.makeText(getApplicationContext(),
                            R.string.uncorrect_password, Toast.LENGTH_SHORT).show();
                    showProgress(false);
                } else {
                    String entryLogin = mLoginView.getText().toString();
                    String entryPassword = mPasswordView.getText().toString();
                    Call<Token> call = serverApi.signIn(new User(entryLogin, entryPassword));
                    call.enqueue(new Callback<Token>() {
                        @Override
                        public void onResponse(@NonNull Call<Token> call, @NonNull Response<Token> response) {
                            showProgress(true);
                            if (response.isSuccessful()) {
                                Token token = response.body();

                                try {
                                    Thread.sleep(2);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }

                                //заносим данные токенов в Shared Preferences
                               PersistentStorage persistentStorage = PersistentStorage.getInstance(getApplicationContext());
                                //PersistentStorage.init(getApplicationContext());
                                persistentStorage.addProperty(getApplicationContext().getString(R.string.tokenA), token.accessToken);
                                persistentStorage.addProperty(getApplicationContext().getString(R.string.tokenR), token.refreshToken);

                                Toast.makeText(getApplicationContext(),
                                        R.string.authorization_success, Toast.LENGTH_SHORT).show();

                                Intent intent = new Intent(getApplicationContext(), AccountInfoActivity.class);
                                intent.putExtra("token", token.accessToken);
                                startActivity(intent);
                                showProgress(false);

                            } else if (response.code() == 401) {

                                Toast.makeText(getApplicationContext(),
                                        getApplicationContext().getString(R.string.incorrect_login_password) + " " + response.code(), Toast.LENGTH_SHORT).show();
                                showProgress(false);
                                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                                startActivity(intent);
                            } else if (response.code() == 500) {

                                Toast.makeText(getApplicationContext(),
                                        getApplicationContext().getString(R.string.server_error) + " " + response.code(), Toast.LENGTH_SHORT).show();
                                showProgress(false);
                                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                                startActivity(intent);
                            } else {
                                showProgress(false);
                                Toast.makeText(getApplicationContext(),
                                        getApplicationContext().getString(R.string.other_mistake) + " " + response.code() + response.body(), Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                                startActivity(intent);
                            }
                        }

                        @Override
                        public void onFailure(@NonNull Call<Token> call, @NonNull Throwable t) {
                            HttpException exception = (HttpException) t;
                            Toast.makeText(getApplicationContext(),
                                    getApplicationContext().getString(R.string.oops_error) + " " + t + exception.code(), Toast.LENGTH_SHORT).show();
                            showProgress(false);
                            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);

                            startActivity(intent);
                        }
                    });
                }
            }
        });

        createAccount.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                showProgress(true);
                Intent intent = new Intent(getApplicationContext(), ActivityRegistration.class);
                startActivity(intent);
                showProgress(false);

            }
        });
    }

    private boolean isEmailValid(String email) {
        return email.length() > 2;
    }

    private boolean isPasswordValid(String password) {
        return password.length() > 4;
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }


    @Override
    public void onBackPressed() {
        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
        startActivity(intent);
    }
}


