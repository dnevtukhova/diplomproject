package com.tinkoffschool.diplomproject.PagingLibrary;

import android.arch.lifecycle.MutableLiveData;
import android.arch.paging.DataSource;
import android.arch.paging.PageKeyedDataSource;

import com.tinkoffschool.diplomproject.Retrofit.HistoryItem;

public class HistoryDataSourceFactory extends DataSource.Factory {
    //creating the mutable live data
    private MutableLiveData<PageKeyedDataSource<Integer, HistoryItem>> itemLiveDataSource = new MutableLiveData<>();

    @Override
    public DataSource<Integer, HistoryItem> create() {
        //getting our data source object
        HistoryDataSource itemDataSource = new HistoryDataSource();

        //posting the datasource to get the values
        itemLiveDataSource.postValue(itemDataSource);

        //returning the datasource
        return itemDataSource;
    }

    //getter for itemlivedatasource
    public MutableLiveData<PageKeyedDataSource<Integer, HistoryItem>> getItemLiveDataSource() {
        return itemLiveDataSource;
    }
}
