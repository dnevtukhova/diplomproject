package com.tinkoffschool.diplomproject.PagingLibrary;

import android.arch.paging.PagedListAdapter;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.tinkoffschool.diplomproject.R;
import com.tinkoffschool.diplomproject.Retrofit.HistoryItem;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


public class PagedAdapterHistory extends PagedListAdapter<HistoryItem, PagedAdapterHistory.HistoryViewHolder> {
   // private HistoryItem[] historyItems;
    private Context context;
    private Date date;

    public PagedAdapterHistory(Context context) {
        super(DIFF_CALLBACK);
      //  this.historyItems = historyItems;
        this.context = context;
    }

    @NonNull
    @Override
    public HistoryViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_view, viewGroup, false);


      //  PagedAdapterHistory.HistoryViewHolder pvh = new PagedAdapterHistory.HistoryViewHolder(v);
        return new PagedAdapterHistory.HistoryViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull HistoryViewHolder historyViewHolder, int i) {

        HistoryItem historyItem = getItem(i);
        if (historyItem != null) {
            historyViewHolder.stockName.setText(historyItem.stock.name);
            Glide.with(context)
                    .load(historyItem.stock.iconUrl)
                    .into(historyViewHolder.stockPhoto);

            String stockCost = (String.format(Locale.US, "%.2f", (Double.parseDouble(
                    historyItem.totalPrice))) + " руб.");

            historyViewHolder.stockCost.setText(stockCost);

String stockCount = (historyItem.stock.code + " - " + historyItem.amount + " шт.");
            historyViewHolder.stockCount.setText(stockCount);
            try {
                date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.US).parse(historyItem.date);
                // System.out.println(date);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            Format formatNew = new SimpleDateFormat("dd.MM.yyyy", Locale.US);
            String dateWithoutTime = formatNew.format(date);
            historyViewHolder.date.setText(dateWithoutTime);
        } else {
            Toast.makeText(context, "Item is null", Toast.LENGTH_LONG).show();
        }

    }


    private static DiffUtil.ItemCallback<HistoryItem> DIFF_CALLBACK =
            new DiffUtil.ItemCallback<HistoryItem>() {
                @Override
                public boolean areItemsTheSame(HistoryItem oldItem, HistoryItem newItem) {
                    return oldItem.transactionId.equals(newItem.transactionId);
                }

                @Override
                public boolean areContentsTheSame(HistoryItem oldItem, @NonNull HistoryItem newItem) {
                    return oldItem.equals(newItem);
                }
            };

    public class HistoryViewHolder extends RecyclerView.ViewHolder {

        CardView cv;
        TextView stockName;
        TextView stockCount;
        ImageView stockPhoto;
        TextView stockCost;
        TextView date;
        ImageView arrow;

        HistoryViewHolder(View itemView) {
            super(itemView);
            cv = itemView.findViewById(R.id.cv);
            stockName = itemView.findViewById(R.id.stock_name);
            stockCount = itemView.findViewById(R.id.stock_quantity);
            stockPhoto = itemView.findViewById(R.id.stock_photo);
            stockCost = itemView.findViewById(R.id.stock_cost);
            date = itemView.findViewById(R.id.cost_changes);
            arrow = itemView.findViewById(R.id.arrow);
        }
    }
}
