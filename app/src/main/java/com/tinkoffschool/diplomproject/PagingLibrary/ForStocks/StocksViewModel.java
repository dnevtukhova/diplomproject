package com.tinkoffschool.diplomproject.PagingLibrary.ForStocks;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;
import android.arch.paging.LivePagedListBuilder;
import android.arch.paging.PageKeyedDataSource;
import android.arch.paging.PagedList;

import com.tinkoffschool.diplomproject.Retrofit.Stocks;

public class StocksViewModel extends ViewModel {

    //creating livedata for PagedList  and PagedKeyedDataSource
    public LiveData<PagedList<Stocks>> itemPagedList;
    public LiveData<PageKeyedDataSource<Integer, Stocks>> liveDataSource;

    //constructor
    public StocksViewModel() {


        //getting our data source factory
        StocksDataSourceFactory itemDataSourceFactory = new StocksDataSourceFactory();

        //getting the live data source from data source factory
        liveDataSource = itemDataSourceFactory.getItemLiveDataSource();

        //Getting PagedList config
        PagedList.Config pagedListConfig =
                (new PagedList.Config.Builder())
                        .setEnablePlaceholders(false)
                        .setPageSize(10)
                        .build();

        //Building the paged list
        itemPagedList = (new LivePagedListBuilder(itemDataSourceFactory, pagedListConfig))
                .build();
    }
}
