package com.tinkoffschool.diplomproject.PagingLibrary.ForStocks;

import android.annotation.SuppressLint;
import android.arch.paging.PagedListAdapter;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.tinkoffschool.diplomproject.FragmentBottomSheetBuy;
import com.tinkoffschool.diplomproject.R;
import com.tinkoffschool.diplomproject.Retrofit.Stocks;

import java.util.Locale;

public class PagedAdapterStocks extends PagedListAdapter<Stocks, PagedAdapterStocks.StockViewHolder> {
  //  private Stocks[] stocks;
    private FragmentManager fragmentManager;

    private RecyclerView recyclerView;
    private Context context;
    private Bundle bundle;
    private String id;
    private String name;
   // private int i1;

//    private void setI1(int i1) {
//        this.i1 = i1;
//    }

   // public int getI1() {
    //    return i1;
  //  }

    public PagedAdapterStocks(FragmentManager fragmentManager, RecyclerView rv, Context context) {

        super(DIFF_CALLBACK);
      //  this.stocks = stocks;
        this.fragmentManager = fragmentManager;
        this.recyclerView = rv;
        this.context = context;
    }

    private static DiffUtil.ItemCallback<Stocks> DIFF_CALLBACK =
            new DiffUtil.ItemCallback<Stocks>() {
                @Override
                public boolean areItemsTheSame(Stocks oldItem, Stocks newItem) {
                    return oldItem.id.equals(newItem.id);
                }

                @Override
                public boolean areContentsTheSame(Stocks oldItem, @NonNull Stocks newItem) {
                    return oldItem.equals(newItem);
                }
            };


    @Override
    @NonNull
    public PagedAdapterStocks.StockViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_view, viewGroup, false);
        PagedAdapterStocks.StockViewHolder pvh = new PagedAdapterStocks.StockViewHolder(v);

      //  setI1(i);
        System.out.println(i + "пришел индекс");
        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int pos = recyclerView.getChildAdapterPosition(view);
                System.out.println(pos + " это позиция!");

                Stocks stocksItem = getItem(pos);
                id = stocksItem.id;
                name = stocksItem.name;


                bundle = new Bundle();
                bundle.putString("id", id);
                bundle.putString("name", name);
                FragmentBottomSheetBuy bottomSheetFragment = FragmentBottomSheetBuy.newInstance();
                bottomSheetFragment.setArguments(bundle);
                bottomSheetFragment.show(fragmentManager, bottomSheetFragment.getTag());
            }
        });

        return pvh;
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(@NonNull PagedAdapterStocks.StockViewHolder stockViewHolder, int i) {
        Stocks stocksItem = getItem(i);
        stockViewHolder.stockName.setText(stocksItem.name);
        Glide.with(context)
                .load(stocksItem.iconUrl)
                .into(stockViewHolder.stockPhoto);
        String stockCost = (String.format(Locale.US, "%.2f", (Double.parseDouble(
                stocksItem.price))) + " руб.");

        stockViewHolder.stockCost.setText(stockCost);

        stockViewHolder.stockCount.setText(stocksItem.code);

        if (Double.parseDouble(stocksItem.priceDelta) > 0) {
            stockViewHolder.costChanges.setTextColor(/*R.color.colorGreen*/Color.parseColor("#48C044"));
            stockViewHolder.arrow.setImageResource(R.drawable.ic_arrow_upward_black_24dp);
            stockViewHolder.arrow.setColorFilter(Color.parseColor("#48C044"));

        } else {
            stockViewHolder.costChanges.setTextColor(Color.parseColor("#ff630f"));
            stockViewHolder.arrow.setImageResource(R.drawable.ic_arrow_downward_black_24dp);
            stockViewHolder.arrow.setColorFilter(Color.parseColor("#ff630f"));

        }
        double priceDelta1 = (Double.parseDouble(stocksItem.priceDelta) * 100) / Double.parseDouble(stocksItem.price);
        String costChanges =  (String.format(Locale.US, "%.2f", (Double.parseDouble(stocksItem.priceDelta))) + " руб. (" + String.format(Locale.US, "%.2f", (priceDelta1)) + "%)");
        stockViewHolder.costChanges.setText(costChanges);

    }


    public static class StockViewHolder extends RecyclerView.ViewHolder {

        CardView cardView;
        TextView stockName;
        TextView stockCount;
        ImageView stockPhoto;
        TextView stockCost;
        TextView costChanges;
        ImageView arrow;

        private StockViewHolder(final View itemView) {
            super(itemView);
            cardView = itemView.findViewById(R.id.cv);
            stockName = itemView.findViewById(R.id.stock_name);
            stockCount = itemView.findViewById(R.id.stock_quantity);
            stockPhoto = itemView.findViewById(R.id.stock_photo);
            stockCost = itemView.findViewById(R.id.stock_cost);
            costChanges = itemView.findViewById(R.id.cost_changes);
            arrow = itemView.findViewById(R.id.arrow);

        }
    }
}
