package com.tinkoffschool.diplomproject.PagingLibrary;

import android.arch.paging.PageKeyedDataSource;
import android.content.Context;
import android.support.annotation.NonNull;
import android.widget.Toast;
import com.tinkoffschool.diplomproject.PersistantStorage.PersistentStorage;
import com.tinkoffschool.diplomproject.R;
import com.tinkoffschool.diplomproject.Retrofit.HistoryItem;
import com.tinkoffschool.diplomproject.Retrofit.TransactionsHistory;

import java.util.Arrays;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.tinkoffschool.diplomproject.PagingLibrary.ContextManager.getContext;


public class HistoryDataSource extends PageKeyedDataSource<Integer, HistoryItem> {


    //we will start from the first page which is 0
    private static final int FIRST_ITEM_ID = 0;

    //получаем токен из персистентного хранилища
private PersistentStorage persistentStorage;

    @Override
    public void loadInitial(@NonNull LoadInitialParams<Integer> params, @NonNull final LoadInitialCallback<Integer, HistoryItem> callback) {
        final Context context = getContext();
       // PersistentStorage.init(context);
        persistentStorage = PersistentStorage.getInstance(getContext());
        String token = persistentStorage.getProperty(getContext().getString(R.string.tokenA));
        Call<TransactionsHistory> call = RetrofitClient.getInstance().getApi().getHistory(token, FIRST_ITEM_ID);
        call.enqueue(new Callback<TransactionsHistory>() {
            @Override
            public void onResponse(@NonNull  Call<TransactionsHistory> call, @NonNull Response<TransactionsHistory> response) {
                if (response.isSuccessful()) {

                    TransactionsHistory stocksHistory = response.body();
                    System.out.println("что-то произошло" + stocksHistory.nextItemId);
                    callback.onResult(Arrays.asList(stocksHistory.items), null, stocksHistory.nextItemId);
                } else {
                    Toast.makeText(context,
                            "Ошибка " + response.code(), Toast.LENGTH_SHORT).show();

                }

            }

            @Override
            public void onFailure(@NonNull Call<TransactionsHistory> call, @NonNull Throwable t) {
                System.out.println("ПРОИЗОШЛА ОШИБКА " + t);

            }
        });
    }

    @Override

    public void loadBefore(@NonNull final LoadParams<Integer> params, @NonNull final LoadCallback<Integer, HistoryItem> callback) {
        final Context context = getContext();
       //
        persistentStorage = PersistentStorage.getInstance(getContext());
        String token = persistentStorage.getProperty(getContext().getString(R.string.tokenA));

        Call<TransactionsHistory> call = RetrofitClient.getInstance().getApi().getHistory(token, params.key);
        call.enqueue(new Callback<TransactionsHistory>() {
            @Override
            public void onResponse(@NonNull Call<TransactionsHistory> call, @NonNull Response<TransactionsHistory> response) {
                if (response.isSuccessful()) {

                    TransactionsHistory stocksHistory = response.body();
                    Integer adjacentKey = (params.key != stocksHistory.prevItemId) ? stocksHistory.prevItemId : null;
                    callback.onResult(Arrays.asList(stocksHistory.items), adjacentKey);
                } else {
                    Toast.makeText(context,
                            "Ошибка " + response.code(), Toast.LENGTH_SHORT).show();

                }

            }

            @Override
            public void onFailure(@NonNull Call<TransactionsHistory> call, @NonNull Throwable t) {

            }
        });
    }

    @Override
    public void loadAfter(@NonNull final LoadParams<Integer> params, @NonNull final LoadCallback<Integer, HistoryItem> callback) {

        final Context context = getContext();
     //   PersistentStorage.init(context);
        persistentStorage =PersistentStorage.getInstance(getContext());
        String token = persistentStorage.getProperty(getContext().getString(R.string.tokenA));
        Call<TransactionsHistory> call = RetrofitClient.getInstance().getApi().getHistory(token, params.key);
        call.enqueue(new Callback<TransactionsHistory>() {
            @Override
            public void onResponse(@NonNull Call<TransactionsHistory> call, @NonNull Response<TransactionsHistory> response) {
                if (response.isSuccessful()) {
                    TransactionsHistory stocksHistory = response.body();
                    //для начала проверим, что prevId!=nextId, тк в противном случае список закончился
                    if (stocksHistory.prevItemId != stocksHistory.nextItemId) {

                        System.out.println(stocksHistory.nextItemId + " " + stocksHistory.prevItemId);

                        Integer keyAfter = (params.key != stocksHistory.nextItemId) ? stocksHistory.nextItemId : null;
                        System.out.println("в load.after " + "key " + params.key + " " + stocksHistory.nextItemId + " " + stocksHistory.prevItemId);
                        callback.onResult(Arrays.asList(stocksHistory.items), keyAfter);

                    }
                } else {
                    Toast.makeText(context,
                            "Ошибка " + response.code(), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
                        public void onFailure(@NonNull Call<TransactionsHistory> call, @NonNull Throwable t) {

            }
        });
//}

    }


}
