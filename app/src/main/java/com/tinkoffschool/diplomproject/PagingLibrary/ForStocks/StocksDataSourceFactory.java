package com.tinkoffschool.diplomproject.PagingLibrary.ForStocks;

import android.arch.lifecycle.MutableLiveData;
import android.arch.paging.DataSource;
import android.arch.paging.PageKeyedDataSource;
import com.tinkoffschool.diplomproject.Retrofit.Stocks;

public class StocksDataSourceFactory extends DataSource.Factory {
    //creating the mutable live data
    private MutableLiveData<PageKeyedDataSource<Integer, Stocks>> itemLiveDataSource = new MutableLiveData<>();

    @Override
    public DataSource<Integer, Stocks> create() {
        //getting our data source object
        StocksDataSource itemDataSource = new StocksDataSource();

        //posting the datasource to get the values
        itemLiveDataSource.postValue(itemDataSource);

        //returning the datasource
        return itemDataSource;
    }


    //getter for itemlivedatasource
    public MutableLiveData<PageKeyedDataSource<Integer, Stocks>> getItemLiveDataSource() {
        return itemLiveDataSource;
    }
}
