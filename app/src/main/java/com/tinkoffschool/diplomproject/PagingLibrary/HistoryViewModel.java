package com.tinkoffschool.diplomproject.PagingLibrary;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;
import android.arch.paging.LivePagedListBuilder;
import android.arch.paging.PageKeyedDataSource;
import android.arch.paging.PagedList;

import com.tinkoffschool.diplomproject.Retrofit.HistoryItem;

public class HistoryViewModel extends ViewModel {

    //creating livedata for PagedList  and PagedKeyedDataSource
    public LiveData<PagedList<HistoryItem>> itemPagedList;
    public LiveData<PageKeyedDataSource<Integer, HistoryItem>> liveDataSource;

    //constructor
    public HistoryViewModel() {


        //getting our data source factory
        HistoryDataSourceFactory itemDataSourceFactory = new HistoryDataSourceFactory();

        //getting the live data source from data source factory
        liveDataSource = itemDataSourceFactory.getItemLiveDataSource();

        //Getting PagedList config
        PagedList.Config pagedListConfig =
                (new PagedList.Config.Builder())
                        .setEnablePlaceholders(false)
                        .setPageSize(20)
                        .build();

        //Building the paged list
        itemPagedList = (new LivePagedListBuilder(itemDataSourceFactory, pagedListConfig))
                .build();
    }
}
