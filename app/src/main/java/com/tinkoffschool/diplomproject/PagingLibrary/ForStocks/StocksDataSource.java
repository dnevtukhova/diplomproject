package com.tinkoffschool.diplomproject.PagingLibrary.ForStocks;

import android.arch.paging.PageKeyedDataSource;
import android.content.Context;
import android.support.annotation.NonNull;
import android.widget.Toast;

import com.tinkoffschool.diplomproject.PagingLibrary.ContextManager;
import com.tinkoffschool.diplomproject.PagingLibrary.RetrofitClient;
import com.tinkoffschool.diplomproject.PersistantStorage.PersistentStorage;
import com.tinkoffschool.diplomproject.R;
import com.tinkoffschool.diplomproject.Retrofit.Stocks;
import com.tinkoffschool.diplomproject.Retrofit.StocksBag;
import java.util.Arrays;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.tinkoffschool.diplomproject.PagingLibrary.ContextManager.getContext;

public class StocksDataSource extends PageKeyedDataSource<Integer, Stocks> {


    //we will start from the first page which is 1
    private static final int FIRST_ITEM_ID = 1;

    //получаем токен из персистентного хранилища
 private PersistentStorage persistentStorage;
  //  private Context context;

    @Override
    public void loadInitial(@NonNull LoadInitialParams<Integer> params, @NonNull final LoadInitialCallback<Integer, Stocks> callback) {
        final Context context = getContext();
      //  PersistentStorage.init(context);
        persistentStorage = PersistentStorage.getInstance(getContext());
        String token = persistentStorage.getProperty(getContext().getString(R.string.tokenA));
        Call<StocksBag> call = RetrofitClient.getInstance().getApi().getStocks(token, FIRST_ITEM_ID);
        call.enqueue(new Callback<StocksBag>() {
            @Override
            public void onResponse(@NonNull Call<StocksBag> call, @NonNull Response<StocksBag> response) {
                if (response.isSuccessful()) {
                    System.out.println("response!! " + response.toString());

                    StocksBag stocksBag = response.body();
                    callback.onResult(Arrays.asList(stocksBag.items), null, stocksBag.nextItemId);
                } else {
                    Toast.makeText(context,
                            "Ошибка " + response.code(), Toast.LENGTH_SHORT).show();

                }

            }

            @Override
            public void onFailure(@NonNull  Call<StocksBag> call, @NonNull Throwable t) {
                System.out.println("ПРОИЗОШЛА ОШИБКА " + t);

            }
        });
    }

    @Override
    public void loadBefore(@NonNull final LoadParams<Integer> params, @NonNull final LoadCallback<Integer, Stocks> callback) {

        final Context context = getContext();
     //   PersistentStorage.init(context);
        persistentStorage = PersistentStorage.getInstance(getContext());
        String token = persistentStorage.getProperty(getContext().getString(R.string.tokenA));
        Call<StocksBag> call = RetrofitClient.getInstance().getApi().getStocks(token, params.key);
        call.enqueue(new Callback<StocksBag>() {
            @Override
            public void onResponse(@NonNull Call<StocksBag> call, @NonNull Response<StocksBag> response) {
                if (response.isSuccessful()) {

                    StocksBag stocksBag = response.body();
                    System.out.println("loadBefore" + params.key);
                    Integer key = (params.key != stocksBag.prevItemId) ? stocksBag.prevItemId : null;
                    callback.onResult(Arrays.asList(stocksBag.items), key);
                } else {
                    Toast.makeText(context,
                            "Ошибка " + response.code(), Toast.LENGTH_SHORT).show();

                }

            }

            @Override
            public void onFailure(@NonNull Call<StocksBag> call, @NonNull Throwable t) {
                System.out.println("ПРОИЗОШЛА ОШИБКА " + t);

            }
        });

    }

    @Override
    public void loadAfter(@NonNull final LoadParams<Integer> params, @NonNull final LoadCallback<Integer, Stocks> callback) {
        if (params.key > 1) {
            final Context context = getContext();
          //  PersistentStorage.init(context);
            persistentStorage = PersistentStorage.getInstance(getContext());
            String token = persistentStorage.getProperty(getContext().getString(R.string.tokenA));
            // String token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE1NTYyNTI3NDcsCiAgInVzZXIiIDogIjEyMzQ1cSIsCiAgInRva2VuVHlwZSIgOiAiYWNjZXNzIgp9.yPxt_kZlobQRkFhL59eOHUDJ-jyLlFxx6Hx635ZtWEs";
            Call<StocksBag> call = RetrofitClient.getInstance().getApi().getStocks(token, params.key);
            call.enqueue(new Callback<StocksBag>() {
                @Override
                public void onResponse(@NonNull Call<StocksBag> call, @NonNull Response<StocksBag> response) {
                    if (response.isSuccessful()) {
                        System.out.println("response!! " + response.toString());
                        StocksBag stocksBag = response.body();
                        System.out.println("after" + params.key);

                        Integer key = params.key != stocksBag.nextItemId ? stocksBag.nextItemId : null;
                        System.out.println("key после" + key);
                        callback.onResult(Arrays.asList(stocksBag.items), key);
                    } else {
                        Toast.makeText(context,
                                "Ошибка " + response.code(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<StocksBag> call, @NonNull Throwable t) {
                    System.out.println("ПРОИЗОШЛА ОШИБКА " + t);
                }
            });
        }
    }
}

