package com.tinkoffschool.diplomproject.Caching;

import android.content.Context;
import android.support.annotation.NonNull;

import com.tinkoffschool.diplomproject.NetWorkUtils.NetWorkUtils;
import java.io.IOException;

import okhttp3.Cache;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;

public class Caching {


    public OkHttpClient CachingFile(final Context appContext) {
       // File dir = new File(appContext.getCacheDir(), "cache_file");
        //константы
        final int MAX_SIZE = 10 * 1024 * 1024; //максимальный размер кэша 10 МБ
        final int MAX_AGE_STALE = 300; //5 минут
        return new OkHttpClient
                .Builder()
                .cache(new Cache(appContext.getCacheDir(), MAX_SIZE)) // 10 MB
                .addInterceptor(new Interceptor() {
                    @Override
                    public okhttp3.Response intercept(@NonNull Chain chain) throws IOException {
                        Request request = chain.request();
                        if (NetWorkUtils.hasConnection(appContext)) {
                            request = request.newBuilder().header("Cache-Control", "public, max-age=" + MAX_AGE_STALE).build();
                        } else {
                            request = request.newBuilder().header("Cache-Control", "public, only-if-cached, max-stale=" + MAX_AGE_STALE).build();
                        }
                        return chain.proceed(request);
                    }
                })
                .build();

    }

}
