package com.tinkoffschool.diplomproject;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.tinkoffschool.diplomproject.Adapter.RVAdapter;
import com.tinkoffschool.diplomproject.Caching.Caching;
import com.tinkoffschool.diplomproject.PagingLibrary.RetrofitClient;
import com.tinkoffschool.diplomproject.PersistantStorage.PersistentStorage;
import com.tinkoffschool.diplomproject.Retrofit.AccountInfo;
import com.tinkoffschool.diplomproject.Retrofit.ServerApi;
import com.tinkoffschool.diplomproject.Retrofit.Stocks;

import java.util.Locale;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.HttpException;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.tinkoffschool.diplomproject.LoginActivity.BASE_URL;

public class AccountInfoActivity extends AppCompatActivity {

    private TextView personName;
  PersistentStorage persistentStorage;
    private ProgressBar progressBarAccountInfo;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // получим идентификатор выбранного пункта меню
        Intent intent = new Intent(getApplicationContext(), ActivityHistory.class);
        startActivity(intent);
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_info);
        //ресайклер вью
        final RecyclerView rv = findViewById(R.id.recyclerView);
        LinearLayoutManager llm = new LinearLayoutManager(getApplicationContext());
        llm.setAutoMeasureEnabled(true);
        rv.setLayoutManager(llm);
        rv.setNestedScrollingEnabled(false);

        //toolbar_account_info
        Toolbar toolbar = findViewById(R.id.toolbar);
       ImageView mImageView = findViewById(R.id.user_photo);
        mImageView.setImageResource(R.mipmap.ic_launcher_person_round);
        personName = findViewById(R.id.user_name);

        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setBackgroundColor(getResources().getColor(R.color.colorGray));
        fab.setImageDrawable(getApplicationContext().getDrawable(R.drawable.ic_add_white_24dp));
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), StocksActivity.class);
                startActivity(intent);
            }
        });

        SwipeRefreshLayout mSwipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);
        progressBarAccountInfo = findViewById(R.id.accountInfo_progress);

        //кешируем
        OkHttpClient client = new Caching().CachingFile(getApplicationContext());

        //загружаем данные
        callApiAccountInfo(client, rv);
        //обработка обновления по свайпу вниз
        onRefreshLayout(mSwipeRefreshLayout, client, rv);

    }

    @Override
    public void onBackPressed() {
        openQuitDialog();
    }

    private void onRefreshLayout(final SwipeRefreshLayout mSwipeRefreshLayout, final OkHttpClient client, final RecyclerView rv) {
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                callApiAccountInfo(client, rv);
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    public void callApiAccountInfo(OkHttpClient client, final RecyclerView rv) {
        showProgress(true);
        //получаем токен из персистентного хранилища
        //PersistentStorage.init(this);
        persistentStorage = PersistentStorage.getInstance(getApplicationContext());
        String token = persistentStorage.getProperty(getApplicationContext().getString(R.string.tokenA));
        System.out.println(token);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
//        RetrofitClient retrofitClient = RetrofitClient.getInstance();


        final ServerApi serverApi = retrofit.create(ServerApi.class);
        Call<AccountInfo> call = serverApi.getAccountInfo(token);

        call.enqueue(new Callback<AccountInfo>() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onResponse(@NonNull Call<AccountInfo> call, @NonNull Response<AccountInfo> response) {
                showProgress(true);

                if (response.isSuccessful()) {
                    System.out.println("response!! " + response.toString());
                    AccountInfo accountInfo = response.body();
                    System.out.println(accountInfo.name);
                    personName.setText(accountInfo.name);
                    personName.setTextColor(R.color.colorBlack);

                    TextView tw = findViewById(R.id.textView2);
                    tw.setText(String.format(Locale.US,"%.2f", (Double.parseDouble(accountInfo.balance))));
                    Stocks[] stocks = accountInfo.stocks;
                    for (int i = 0; i < stocks.length; i++) {
                        System.out.println(stocks[i].code + " " + stocks[i].count + " " + stocks[i].id + " " + stocks[i].price + " " + stocks[i].priceDelta + " " + stocks[i].name + " ");
                    }
                    RVAdapter rvAdapter = new RVAdapter(stocks, getSupportFragmentManager(), rv, getApplicationContext());
                    rv.setAdapter(rvAdapter);
                    rvAdapter.notifyDataSetChanged();
                }
                showProgress(false);
            }

            @Override
            public void onFailure( @NonNull Call<AccountInfo> call,@NonNull Throwable t) {
                HttpException exception = (HttpException) t;

                System.out.println("failure!!!! " + exception.code());
                showProgress(false);
            }


        });

    }

    private void showProgress(boolean visible) {
        progressBarAccountInfo.setVisibility(visible ? View.VISIBLE : View.INVISIBLE);
    }

    private void openQuitDialog() {
        AlertDialog.Builder quitDialog = new AlertDialog.Builder(
                this);
        quitDialog.setTitle(R.string.are_you_shure);

        quitDialog.setPositiveButton(R.string.name_yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                persistentStorage.removeProperty();

                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(intent);
                finish();
            }
        });

        quitDialog.setNegativeButton(R.string.name_no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub

            }
        });

        quitDialog.show();
    }
}



