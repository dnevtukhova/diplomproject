package com.tinkoffschool.diplomproject.MyApp;

import android.app.Application;
import com.squareup.leakcanary.LeakCanary;
import com.tinkoffschool.diplomproject.PersistantStorage.PersistentStorage;

public class MyApp extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        LeakCanary.install(this);
        PersistentStorage.getInstance(getApplicationContext());
    }
}
