package com.tinkoffschool.diplomproject.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.tinkoffschool.diplomproject.FragmentBottomSheetBuy;
import com.tinkoffschool.diplomproject.FragmentBottomSheetSel;
import com.tinkoffschool.diplomproject.R;
import com.tinkoffschool.diplomproject.Retrofit.Stocks;

import java.util.Locale;

public class RVAdapter extends RecyclerView.Adapter<RVAdapter.StockViewHolder> {
    private Stocks[] stocks;
    private FragmentManager fragmentManager;

    private RecyclerView recyclerView;
    private Context contextInAdapter;

    public RVAdapter(Stocks[] stocks, FragmentManager fragmentManager, RecyclerView recyclerView, Context context) {
        this.stocks = stocks;
        this.fragmentManager = fragmentManager;

        this.recyclerView = recyclerView;
        this.contextInAdapter = context;
    }


    @Override
    @NonNull
    public StockViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_view, viewGroup, false);
        StockViewHolder pvh = new StockViewHolder(v);

        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int pos = recyclerView.getChildAdapterPosition(view);
                System.out.println(pos + " это позиция!");
                String id = stocks[pos].id;
                System.out.println("!!!!!" + stocks[pos].id);

                //Получаем наименование акции
                String name = stocks[pos].name;
                System.out.println("name акции" + stocks[pos].name);

                //Передаем во фрагмент id и name
                Bundle bundle = new Bundle();
                bundle.putString("id", id);
                bundle.putString("name", name);
                bundle.putString("price", stocks[pos].price);
                if (stocks[pos].count == null) {
                    FragmentBottomSheetBuy bottomSheetFragment = FragmentBottomSheetBuy.newInstance();
                    bottomSheetFragment.setArguments(bundle);
                    bottomSheetFragment.show(fragmentManager, bottomSheetFragment.getTag());
                } else {

                    FragmentBottomSheetSel bottomSheetFragment = FragmentBottomSheetSel.newInstance();
                    bottomSheetFragment.setArguments(bundle);
                    bottomSheetFragment.show(fragmentManager, bottomSheetFragment.getTag());
                }
            }

        });

        return pvh;
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(@NonNull StockViewHolder stockViewHolder, int i) {
        stockViewHolder.stockName.setText(stocks[i].name);
        Glide.with(contextInAdapter)
                .load(stocks[i].iconUrl)

                .into(stockViewHolder.stockPhoto);
        String stockCost =(String.format(Locale.US,"%.2f", (Double.parseDouble(
                stocks[i].price))) + " руб.");
        String stockCount  = (stocks[i].count + " шт.");

        stockViewHolder.stockCost.setText(stockCost);
        if (stocks[i].count == null) {
            stockViewHolder.stockCount.setText(stocks[i].code);
        } else {
            stockViewHolder.stockCount.setText(stockCount);
        }

        if (Double.parseDouble(stocks[i].priceDelta) > 0) {
            stockViewHolder.costChanges.setTextColor(/*R.color.colorGreen*/Color.parseColor("#48C044"));
            stockViewHolder.arrow.setImageResource(R.drawable.ic_arrow_upward_black_24dp);
            stockViewHolder.arrow.setColorFilter(Color.parseColor("#48C044"));

        } else {
            stockViewHolder.costChanges.setTextColor(Color.parseColor("#ff630f"));
            stockViewHolder.arrow.setImageResource(R.drawable.ic_arrow_downward_black_24dp);
            stockViewHolder.arrow.setColorFilter(Color.parseColor("#ff630f"));

        }
        double priceDelta1 = (Double.parseDouble(stocks[i].priceDelta) * 100) / Double.parseDouble(stocks[i].price);
        String costChanges = (String.format(Locale.US,"%.2f", (Double.parseDouble(stocks[i].priceDelta))) + " руб. (" + String.format(Locale.US, "%.2f", (priceDelta1)) + "%)");
        stockViewHolder.costChanges.setText(costChanges);

    }

    @Override
    public int getItemCount() {
        return stocks.length;
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public static class StockViewHolder extends RecyclerView.ViewHolder {

        CardView cardView;
        TextView stockName;
        TextView stockCount;
        ImageView stockPhoto;
        TextView stockCost;
        TextView costChanges;
        ImageView arrow;

        private StockViewHolder(final View itemView) {
            super(itemView);
            cardView = itemView.findViewById(R.id.cv);
            stockName = itemView.findViewById(R.id.stock_name);
            stockCount = itemView.findViewById(R.id.stock_quantity);
            stockPhoto = itemView.findViewById(R.id.stock_photo);
            stockCost = itemView.findViewById(R.id.stock_cost);
            costChanges = itemView.findViewById(R.id.cost_changes);
            arrow = itemView.findViewById(R.id.arrow);
        }
    }
}

