package com.tinkoffschool.diplomproject;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.arch.paging.PagedList;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.tinkoffschool.diplomproject.PagingLibrary.ContextManager;
import com.tinkoffschool.diplomproject.PagingLibrary.HistoryViewModel;
import com.tinkoffschool.diplomproject.PagingLibrary.PagedAdapterHistory;
import com.tinkoffschool.diplomproject.PersistantStorage.PersistentStorage;
import com.tinkoffschool.diplomproject.Retrofit.HistoryItem;

public class ActivityHistory extends AppCompatActivity {
    private PersistentStorage persistentStorage;
  //  private Toolbar toolbar;
   // private TextView textView;
   // private ImageButton buttonBackHistory;
   private ProgressBar progressBarHistory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);
        ContextManager.setContext(this);
       // Toolbar toolbar = findViewById(R.id.toolbar_history);

        TextView textView = findViewById(R.id.name_page);
        textView.setText(R.string.name_history_transactions);

        progressBarHistory = findViewById(R.id.history_progress);

        ImageButton buttonBackHistory = findViewById(R.id.back_button);
        buttonBackHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), AccountInfoActivity.class);
                startActivity(intent);
            }
        });
        showProgress(true);
        //получаем токен из персистентного хранилища
       // PersistentStorage.init(this);
        persistentStorage = PersistentStorage.getInstance(getApplicationContext());
       String token = persistentStorage.getProperty(getApplicationContext().getString(R.string.tokenA));
        final RecyclerView rv = findViewById(R.id.rvHistory);
        LinearLayoutManager llm = new LinearLayoutManager(getApplicationContext());
        rv.setLayoutManager(llm);

        //getting our ItemViewModel
        HistoryViewModel historyViewModel = ViewModelProviders.of(this).get(HistoryViewModel.class);

        //creating the Adapter
        final PagedAdapterHistory adapter = new PagedAdapterHistory(this);


        //observing the itemPagedList from view model
        historyViewModel.itemPagedList.observe(this, new Observer<PagedList<HistoryItem>>() {
            @Override
            public void onChanged(@Nullable PagedList<HistoryItem> items) {

                //in case of any changes
                //submitting the items to adapter
                //  showProgress(true);
                adapter.submitList(items);
                showProgress(false);
            }
        });
        // showProgress(false);

        //setting the adapter

        rv.setAdapter(adapter);
        showProgress(false);

    }

    private void showProgress(boolean visible) {
        progressBarHistory.setVisibility(visible ? View.VISIBLE : View.INVISIBLE);
        System.out.println("пришло" + visible);
    }
}

