package com.tinkoffschool.diplomproject.Retrofit;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ServerApi {
    @POST("/api/auth/signup")
    Call<Token> signUp(
            @Body User user
    );

    @POST("/api/auth/signin")
    Call<Token> signIn(
            @Body User user
    );

    @POST("/api/auth/refresh")
    Call<Token> refresh(
            @Body RefreshToken refreshToken
    );

    @GET("/api/account/info")
    Call<AccountInfo> getAccountInfo(

            @Header("Authorization") String authHeader
    );

    @GET("/api/stocks")
    Call<StocksBag> getStocks(
            @Header("Authorization") String authHeader,
            @Query("itemId") Integer itemID

    );

    @GET("/api/stocks")
    Call<StocksBag> getStocksSearch(
            @Header("Authorization") String authHeader,
            @Query("count") Integer param1,
            @Query("search") String param2
    );

    @GET("/api/transaction/history")
    Call<TransactionsHistory> getHistory(
            @Header("Authorization") String authHeader,
            @Query("itemId") Integer itemId

    );


    @POST("/api/transaction/buy")
    Call<TransactionsSellBuy> transactionBuy(

            @Header("Authorization") String authHeader,
            @Body StockIdAmount stockIdAmount


    );

    @POST("/api/transaction/sell")
    Call<TransactionsSellBuy> transactionSell(
            @Header("Authorization") String authHeader,
            @Body StockIdAmount stockIdAmount

    );


}
