package com.tinkoffschool.diplomproject.Retrofit;

public class StocksBag {
    public int nextItemId;
    public int prevItemId;
    public Stocks[] items;

    public StocksBag(int nextItemId, int preItemId, Stocks[] items) {
        this.nextItemId = nextItemId;
        this.prevItemId = preItemId;
        this.items = items;

    }
}
