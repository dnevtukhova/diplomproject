package com.tinkoffschool.diplomproject.Retrofit;

public class StockIdAmount {
    public Integer stockId;
    public Integer amount;

    public StockIdAmount(Integer stockId, Integer amount) {
        this.stockId = stockId;
        this.amount = amount;

    }
}
