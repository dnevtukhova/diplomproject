package com.tinkoffschool.diplomproject.Retrofit;


public class Token {

    public String accessToken;
    public String refreshToken;

    public Token(String accessToken, String refreshToken) {
        this.accessToken = accessToken;
        this.refreshToken = refreshToken;
    }
}
