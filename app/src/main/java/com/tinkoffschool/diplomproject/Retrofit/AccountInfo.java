package com.tinkoffschool.diplomproject.Retrofit;

public class AccountInfo {
    public String name;
    public String balance;
    public Stocks[] stocks;

    public AccountInfo(String name, String balance) {
        this.name = name;
        this.balance = balance;
    }
}
