package com.tinkoffschool.diplomproject.Retrofit;

public class Stocks {

    public String id;
    public String code;
    public String name;
    public String iconUrl;
    public String price;
    public String priceDelta;
    public String count;

    public Stocks(String id, String code, String name, String iconUrl, String price, String priceDelta, String count) {
        this.id = id;
        this.code = code;
        this.name = name;
        this.iconUrl = iconUrl;
        this.price = price;
        this.priceDelta = priceDelta;

        this.count = count;
    }

}
